
from flask import Blueprint

from flask import Blueprint,request
from dentme.chapters.models import Chapter
from marshmallow import fields
from dentme.extensions import db,docs
import click
from datetime import datetime
from flask_apispec import marshal_with, use_kwargs

blueprint = Blueprint('chapters', __name__)


@blueprint.route('/api/books/chapters/', methods=('GET',))
@use_kwargs({'book_id': fields.Int()})
def get_chapter_from_book():
    args = request.args

    chapFromBook = Chapter.query.filter_by(book_id=args['book_id']).all()
    results = [
        {
            "name": chapter.name,
            "content": chapter.content,
            "book_id": chapter.book_id


        } for chapter in chapFromBook
    ]

    return {
        "count": len(results), "chapFromBook": results
    }

@blueprint.route('/api/chapter/', methods=('GET',))
@use_kwargs({'chap_id': fields.Int()})
def get_chap_content():
    args = request.args
    if 'chap_id' in args:
        chapContent = Chapter.query.filter_by(id=args['chap_id']).first()
    results = chapContent.serialize()



    return results

@blueprint.route('/api/chapters/', methods=('GET',))
def get_allchap():
    chapters = Chapter.query.all()
    results = [
        {
            "name": chapter.name,
            "content": chapter.content,
            "book_id": chapter.book_id

        } for chapter in chapters
    ]

    # print(result, flush=True)
    return {
        "count": len(results), "books": results
    }
docs.register(get_chapter_from_book, blueprint='chapters')
docs.register(get_allchap, blueprint='chapters')
docs.register(get_chap_content, blueprint='chapters')

