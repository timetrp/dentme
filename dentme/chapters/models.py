from sqlalchemy.dialects.postgresql import JSON

# from dentme.quizzes.models import Quiz
from flask_sqlalchemy import SQLAlchemy

from dentme.extensions import db


class Chapter(db.Model):
    __tablename__ = 'chapter'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    name = db.Column(db.String())
    content = db.Column(db.Text)
    book_id = db.Column(db.Integer, db.ForeignKey('book.id'),
                        nullable=False)
    quiz = db.relationship('Quiz', backref='chapter', lazy=True)

    def __init__(self, name, content):
        self.name = name
        self.content = content


    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'content': self.content,
            'book_id': self.book_id
        }
