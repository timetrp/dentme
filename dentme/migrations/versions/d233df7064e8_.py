"""empty message

Revision ID: d233df7064e8
Revises: 96921e54f934
Create Date: 2021-01-12 01:59:21.704198

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd233df7064e8'
down_revision = '96921e54f934'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('quiz', sa.Column('is_publish', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('quiz', 'is_publish')
    # ### end Alembic commands ###
