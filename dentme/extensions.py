from flask_sqlalchemy import SQLAlchemy
from faker import Faker
from flask_apispec.extension import FlaskApiSpec


db = SQLAlchemy()
fake = Faker()
docs = FlaskApiSpec()
