import os
from apispec import APISpec
from flask_cors import CORS
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from flask_apispec.extension import FlaskApiSpec
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy

from .config import Config
from dentme import books, chapters, quizzes\
    # , quizzes
from dentme.books.models import Book
from dentme.chapters.models import Chapter
from dentme.quizzes.models import Quiz
from flask_migrate import Migrate
from .extensions import db,docs
import click
from flask.cli import AppGroup
from flask.json import jsonify
from marshmallow import Schema, fields
from dentme.books.views import create_book
from dentme.quizzes.views import get_quizzes

migrate = Migrate()
#export DATABASE_URL="postgres://developer:dozo2020@127.0.0.1:5432/dentme"

def create_app():

    app = Flask(__name__)
    CORS(app)
    app.config.from_object(Config)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    register_blueprint(app)
    register_extensions(app)
    init_spec(app)
    # db.drop_all()
    # db.create_all()

    migrate.init_app(app, db)
    return app


def register_blueprint(app):
    app.register_blueprint(books.views.blueprint)
    app.register_blueprint(chapters.views.blueprint)
    app.register_blueprint(quizzes.views.blueprint)


def register_extensions(app):
    db.init_app(app)

def init_spec(app):

    app.config.update({
#         'APISPEC_SPEC': APISpec(
#     title="Swagger Example",
#     version="v1",
#     openapi_version="3.0.2",
#     plugins=[FlaskPlugin(), MarshmallowPlugin()],
# ),
        'APISPEC_SWAGGER_URL': '/swagger/',
    })
    docs.init_app(app)
   


