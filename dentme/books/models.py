from flask_sqlalchemy import SQLAlchemy

from dentme.chapters.models import Chapter
# from dentme import app
from dentme.extensions import db


class Book(db.Model):
    __tablename__ = 'book'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String())
    content = db.Column(db.Text)
    upload_date = db.Column(db.DateTime)
    chapters = db.relationship(Chapter, backref='book', lazy=True)

    def __init__(self, name, content, upload_date):
        self.name = name
        self.content = content
        self.upload_date = upload_date

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'content': self.content,
            'name': self.name,
            'uploaded_date': self.upload_date
        }

    # book schema

