import subprocess
from flask import Blueprint, json, jsonify, request, make_response
from .utils import BookContent
from dentme.books.models import Book
from dentme.chapters.models import Chapter
from flask_sqlalchemy import SQLAlchemy
from dentme.extensions import db,docs
from datetime import datetime

import click

blueprint = Blueprint('books', __name__, cli_group="book")


#  id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String())
#     content = db.Column(db.Text)
#     book_id = db.Column(db.Integer, db.ForeignKey('book.id'),
#                         nullable=False)

# id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String())
#     content = db.Column(db.Text)
#     chapters = db.relationship('Chapter', backref='book', lazy=True)

@blueprint.route('/api/books', methods=('POST',))
def create_book():
    # print(request.json, flush=True)
    file = request.files['file']
    content = file.read()
    content = str(content, 'utf-8')
    name = file.filename
    print(content,flush=True)
    # cont_arr = content.split(',')
    # print(name, content, cont_arr, flush=True)

    book = Book(name, content, datetime.now())

    BookChaps = BookContent(name, content,nochapters=False, stats=False)

    # print(content,flush=True)

    print(BookChaps.chapters,flush=True)
    chapters = []
    cont_arr = BookChaps.chapters

    for i, cont in enumerate(cont_arr):
        chapter = Chapter(str(i), cont)
        chapters.append(chapter)
        book.chapters.append(chapter)

    print(chapters, flush=True)
    db.session.add(book)
    db.session.add_all(chapters)
    db.session.commit()
    # print(jsonify(book.serialize()),flush=True)
    return book.serialize()


@blueprint.route('/api/books', methods=('GET',))
def get_books():
    books = Book.query.all()
    results = [
        {
            "name": book.name,
            "content": book.content,
            "uploaded_date": book.upload_date

        } for book in books
    ]

    print(books, flush=True)
    # print(result, flush=True)
    return {
        "count": len(results), "books": results
    }


@blueprint.cli.command('mock')
@click.argument('id')
def create(id):
    book = Book(name='mock_book', content='mock_content', upload_date=datetime.now())

    with open('seed_content.txt') as f:
        content = f.read()
        chapter = Chapter(name='mock_chapter', content=content)
        db.session.add(book)
        db.session.add(chapter)

        book.chapters.append(chapter)
        db.session.commit()



docs.register(create_book, blueprint='books')
docs.register(get_books, blueprint='books')