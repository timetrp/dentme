from flask import Blueprint,request
from dentme.chapters.models import Chapter
from dentme.quizzes.models import Quiz
# from .utils import generate_qa
from dentme.extensions import db,docs
from flask_apispec import marshal_with, use_kwargs, doc
from marshmallow import fields
from datetime import datetime
from dentme.books.models import Book

import json
import click

blueprint = Blueprint('quizzes', __name__, cli_group="quiz")


@doc(tags=['quiz'])
@blueprint.route('/api/quiz', methods=('GET',))
@use_kwargs({'chapter_id': fields.Int(), 'limit': fields.Int(), 'offset': fields.Int()})
def get_quizzes(chapter_id=None,limit=20, offset=0):
    query = Quiz.query
    args = request.args

    if "chapter_id" in args:
        query = query.filter(Quiz.chapter_id == args['chapter_id'])
    quizzes = query.offset(offset).limit(limit).all()
    # print(quizzes, flush=True)
    resp = [ quiz.serialize() for quiz in quizzes]
    # print(resp, flush=True)
    return resp


@doc(tags=['quiz'])
@blueprint.route('/api/c', methods=('GET',))
def get_all_quiz():
    quizzes = Quiz.query.all()
    print(quizzes, flush=True)
    resp = [ quiz.serialize() for quiz in quizzes]
    return json.dumps(resp)


# @blueprint.route('/api/quiz', methods=('POST',))
# def create_qa():
#     # print(request.json, flush=True)
#     print('generating.... please wait 10 hours', flush=True)
#     data = request.json
#     id = data['chapter_id']
#     chapter = Chapter.query.filter_by(id=id).first()
#     content = chapter.content
#     # content = ''
#     quizzes = generate_qa(content)
#     quizs = []
#     for quiz_info in quizzes:
#         quiz_json = json.dumps(quiz_info)
#         quiz = Quiz(question=quiz_info['question'], answer=quiz_info['answer'], choices=quiz_info['choices'],
#          options=quiz_info['options'])
#         chapter.quiz.append(quiz)
#         db.session.add(quiz)
#     db.session.commit()

#     return {"msg": "generated QA for chapter" + str(id)}
# #
docs.register(get_quizzes, blueprint='quizzes')

@blueprint.cli.command('mock')
@click.argument('id')
def create(id):
    # book = Book(name='mock_book', content='mock_content', upload_date=datetime.now())

    chapter = Chapter.query.filter_by(id=id).first()
    
   
    # quiz_json = json.dumps(quiz_info)
    #fdsfsdf fjdf jkjk fdfkdkf
    for i in range(10):
        quiz = Quiz(question='mock question #' + i, answer='ddddd', choices=['a','b,','c', 'd'],
        options=['a'])
        chapter.quiz.append(quiz)
        db.session.add(quiz)


    db.session.commit()



@blueprint.cli.command('seed')
def seed_data():
    book = Book(name='mock_book', content='mock_content', upload_date=datetime.now())

    with open('seed_content.txt') as f:
        content = f.read()
        chapter = Chapter(name='mock_chapter', content=content)
        for i in range(10):
            quiz = Quiz(question='mock question #' + str(i), answer='ddddd', choices=['a','b,','c', 'd'],
            options=['a'])
            chapter.quiz.append(quiz)
            db.session.add(quiz)


        db.session.add(book)
        db.session.add(chapter)

        book.chapters.append(chapter)
        db.session.commit()
