from sqlalchemy.dialects.postgresql import JSON, ARRAY


from flask_sqlalchemy import SQLAlchemy

from dentme.extensions import db


class Quiz(db.Model):
    __tablename__ = 'quiz'

    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String())
    answer = db.Column(db.String())
    choices = db.Column(ARRAY(db.String()))
    options = db.Column(ARRAY(db.String()))
    is_publish = db.Column(db.Boolean)
    # choices = db.Column(MutableList.as_mutable(db.ARRAY(db.String(100))))

    chapter_id = db.Column(db.Integer, db.ForeignKey('chapter.id'),
                        nullable=False)

    def __init__(self, question, answer, choices, options):
        self.question = question
        self.answer = answer
        self.choices = choices
        self.options = options
        self.is_publish = False

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'question': self.question,
            'answer': self.answer,
            'choices': self.choices,
            'options': self.options,
            'is_publish': self.is_publish,
            'chapter_id': self.chapter_id
        }

    def get_quiz_no_ans(self):
        return {
            'id': self.id,
            'question': self.question,
            # 'answer': self.answer,
            'choices': self.choices,
            'options': self.options,
        }

