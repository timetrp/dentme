from flask_sqlalchemy import SQLAlchemy

from dentme.chapters.models import Chapter
# from dentme import app
from dentme.extensions import db


class Student(db.Model):
    __tablename__ = 'student'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String())
    bookProgress = db.Column(db.Integer)
    achievement = db.Column(db.String)
    score = db.Column(db.Integer)

    def __init__(self, name):
        self.name = name


    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'bookProgress': self.bookProgress,
            'achievement': self.achievement,
            'score': self.score
        }

    # book schema

